'use strict';

angular.module('app')

.factory('Helper',
  ['APP', 'lodash', '$filter',
  function(APP, lodash, $filter) {
    var service = {};

    service.ucfirstWord = function(collections, attribute) {
      attribute = attribute ? attribute : '';
      lodash.each(collections, function(data) {
        data[attribute] = $filter('lowercase')(data[attribute]);
        data[attribute] = $filter('ucfirst')(data[attribute]);
      });
      return collections;
    };

    service.addBrackets = function(word) {
      if (word) {
        var wordWithBracket = '(' + word + ')';
        return wordWithBracket;
      }
      else {
        return '';
      }
    };

    service.renameKey = function(collections, key, newKey) {
      lodash.each(collections, function(c) {
        c[newKey] = angular.copy(c[key]);
        delete c[key];
      });
      return collections;
    }

    service.toFloat = function(number) {
      number = number ? number : 0;
      var float = parseFloat(parseFloat(number).toFixed(2));
      return float;
    };

    service.toNumber = function(number) {
      if (number != null) {
        var float = parseFloat(parseFloat(number).toFixed(2));
        return float;
      }
      else {
        return null;
      }
    };

    service.setFullnameDesa = function(desa) {
      lodash.each(desa, function(d) {
        var kelurahan = $filter('lowercase')(d.kel_nama);
        kelurahan = $filter('ucfirst')(kelurahan);
        var kecamatan = $filter('lowercase')(d.kec_nama);
        kecamatan = $filter('ucfirst')(kecamatan);
        var sumberDana = service.addBrackets(d.sumber_dana);
        var namaLengkap = kelurahan + ', Kec. ' + kecamatan + ' ' + sumberDana;
        d.kel_kec_nama = namaLengkap;
      });
      return desa;
    };

    service.normalText = function(word) {
      if (word) {
        var hasUnderscore = /_|-/g.test(word);
        var hasDot = /\./g.test(word);

        if (hasUnderscore || hasDot) {
          var removeUnderscore = word.replace(/_|-/g, " ");
          var removeDot = removeUnderscore.replace(/\./g, " ");
          var finalWord = $filter('ucfirst')(removeDot);
          return finalWord;
        }
        else {
          var finalWord = $filter('ucfirst')(word);
          return finalWord;
        }
      }
    };

    // service.isHomogenous = function(array) {
    //   if (array.length > 0) {
    //     return array.every( (val, i, arr) => val === arr[0] );
    //   }
    //   else {
    //     return false;
    //   }
    // };

    service.isHomogenous = function(array, value) {
      if (array.length > 0) {
        return array.every( (val, i, arr) => arr[i] === value );
      }
      else {
        return false;
      }
    };

    return service;
  }]);
