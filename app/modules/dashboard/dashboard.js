'use strict';

angular.module('app')

.config(['$stateProvider', function($stateProvider) {
  $stateProvider.state('app.dashboard', {
    url: '/dashboard',
    templateUrl: 'modules/dashboard/dashboard.html',
    controller: 'DashboardCtrl'
  });
}])

.controller('DashboardCtrl', ['$rootScope', '$scope',
  function($rootScope, $scope) {
    $rootScope.title = 'Dashboard';
  }]);
