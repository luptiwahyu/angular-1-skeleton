'use strict';

angular.module('app')

.factory('AuthService',
  ['$http', '$cookies', '$rootScope', '$timeout', 'APP', '$q',
  function($http, $cookies, $rootScope, $timeout, APP, $q) {
    var service = {};

    service.login = function(data, onSuccess, onError) {
      var url = APP.BASE_URL + '/login';
      $http.post(url, data).then(onSuccess, onError);
    };

    service.setCredentials = function(data) {
      var currentUser = data.data;
      if (currentUser.user_level == 'PMAC') {
        // currentUser.user_name = 'Provinsi';
        currentUser.user_name = currentUser.user_name.replace('DMA ', '');
      }
      $rootScope.currentUser = currentUser;
      $http.defaults.headers.common.Authorization = 'Basic ' + data.api_token;
      $cookies.put('token', data.api_token);
    };

    service.isAuthenticated = function() {
      return $cookies.get('token') !== undefined;
    };

    service.clearCredentials = function() {
      $rootScope.currentUser = undefined;
      $cookies.remove('token');
      $http.defaults.headers.common.Authorization = 'Basic ';
    };

    service.logout = function(onSuccess, onError) {
      $http.get(APP.BASE_URL + '/logout').then(onSuccess, onError);
    };

    service.currentUser = function() {
      var deferred = $q.defer();
      $http.get(APP.BASE_URL + '/current_user').then(function(response) {
        var user = response.data;
        if (user.user_level == 'PMAC') {
          // user.user_name = 'Provinsi';
          user.user_name = user.user_name.replace('DMA ', '');
        }
        deferred.resolve(user);
      },
      function() {
        deferred.resolve(null);
      });
      return deferred.promise;
    };

    return service;
  }]);
