'use strict';

angular.module('app')

.config(['$stateProvider', function($stateProvider) {
  $stateProvider.state('app.auth', {
    url: '/auth',
    templateUrl: 'modules/auth/auth.html',
    controller: 'AuthCtrl'
  });
}])

.controller('AuthCtrl', ['$rootScope', '$scope', 'AuthService', '$state',
  function($rootScope, $scope, AuthService, $state) {
    $rootScope.title = 'Login';
    $scope.data = {};

    function _validate(data) {
      var validation = {
        status: true,
        messages: [],
      };

      if (!data.userlogin || !data.password) {
        validation.messages.push('User dan password harus diisi');
        validation.status = false;
      }

      return validation;
    }

    $scope.login = function(data) {
      var validation = _validate(data);
      if (validation.status) {
        AuthService.login({data: data}, function(response) {
          AuthService.setCredentials(response.data);
          $state.go('app.dashboard');
        },
        function(error) {
          $scope.errors = [error.data.message];
        });
      }
      else {
        $scope.errors = validation.messages;
      }
    };
  }]);
