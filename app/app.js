'use strict';

angular.module('app', [
  'ui.router',
  'ngCookies',
  'ngSanitize',
  'ui.select',
  'angular.filter',
  'ngLodash',
  'angularify.semantic',
  'angularMoment',
  'ngConfirm',
  'ui.utils.masks',
  'ngFileSaver',
])

.constant('APP', {
  BASE_URL: 'http://localhost:3000',
  API_VERSION: 'v1',
  NAME: 'My Application',
  VERSION: '0.0.1',
})

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/auth');
    $urlRouterProvider.when('', '/auth');
    // $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
  }])

.run(['$rootScope', '$http', '$location', '$cookies', '$state',
  function ($rootScope, $http, $location, $cookies, $state) {
    $http.defaults.headers.common.Authorization = 'Basic ' + $cookies.get('token');
  }]);
