'use strict';

angular.module('app')

.config(['$stateProvider', function($stateProvider) {
  $stateProvider.state('app', {
    abstract: true,
    resolve: {
      currentUser: function(AuthService) {
        return AuthService.isAuthenticated() ? AuthService.currentUser() : undefined;
      }
    },
    views: {
      'header': {
        templateUrl: 'components/header/header.html',
        controller: 'MainCtrl'
      },
      'content': {
        template:'<div ui-view class="header-space"></div>'
      }
    }
  });
}])

.controller('MainCtrl', ['$rootScope', '$scope', '$state', 'AuthService', 'currentUser', '$timeout', 'SiklusService', 'Helper', 'lodash', '$location',
  function($rootScope, $scope, $state, AuthService, currentUser, $timeout, SiklusService, Helper, lodash, $location) {
    $rootScope.currentUser = currentUser;
    $rootScope.addBrackets = Helper.addBrackets;

    init();

    function init() {
      getAllSiklus();
    }

    $scope.logout = function() {
      AuthService.logout(function(response) {
        AuthService.clearCredentials();
        $scope.openAlert();

        $timeout(function() {
          $scope.closeAlert();
          $state.go('app.report');
        }, 2000);
      },
      function(error) {});
    };

    $scope.toLogin = function(filter) {
      $rootScope.filter = filter;
    };

    $scope.openAlert = function() {
      $scope.logoutModal = true;
    };

    $scope.closeAlert = function() {
      $scope.logoutModal = false;
    };

    function getAllSiklus() {
      var params = {};

      SiklusService.getAllSiklus(params, function(response) {
        $rootScope.upSiklus = response.data;
        $rootScope.selectedSiklus = lodash.find($rootScope.upSiklus, {id: 4});
      },
      function(argument) {});
    }

    $rootScope.activeMenu = function (path) {
      return ($location.path().substr(0, path.length) === path) ? 'active grey' : '';
    };

  }]);
